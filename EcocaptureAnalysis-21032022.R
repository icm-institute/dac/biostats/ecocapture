#**********************************
#*  ECOCAPTURE Analysis 2022
#*  (Peltier et al., 2022)
#*  A temporal classification method based on behavior time series data
#*  in patients with behavioral variant of frontotemporal dementia and apathy.
#*  ICM / Institut du Cerveau / Paris Brain Institute
#*  Caroline Peltier, François-Xavier Lejeune, Bénédicte Batrancourt
#*  Contacts:  caroline.peltier@inrae.fr, f-x.lejeune@icm-institute.org
#*  benedicte.batrancourt@upmc.fr / b.batrancourt@icm-institute.org
#*  21st March 2022
#**********************************
#*
#---------------------------------
# loading functions & libraries
#--------------------
rm(list=ls())
library(smacof)
library(ggplot2)
library(gridExtra)
library(stringdist)
library(openxlsx)
library(dtw)
library(eccptrk)

#------------------
# Loading data
#-------------------
#setwd("Path Directory")
datasetNPSI2=as.data.frame(read.xlsx("DATABASE ECOCAPTURE - 20 FTD 18 HC - 21032022.xlsx"))
dataset_raw=as.data.frame(read.xlsx("ECOCAPTURE - Events Log - Coding of Behavior from video - 20 FTD 18 HC - 21032022.xlsx"))


#------------------
# First statistics
#------------------
# TYPE: HC = Healthy Control; FTD = Frontotemporal Dementia
summary(factor(datasetNPSI2[,"TYPE"]))
# Male = 1; Female = 2
summary(factor(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD","SEX"]))
summary(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD","AGE"])
# YOE = Years Of Education
summary(as.numeric(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD","YOE"]))
summary(factor(datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC","SEX"]))

summary(datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC","AGE"])
summary(as.numeric(datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC","YOE"]))

dataset=dataset_raw
dataset[,"Subject"]=as.factor(dataset[,"Subject"])
subjects_all=levels(factor(as.character(dataset[dataset[,"Subject"]!="","Subject"])))


#------------------------------------------------
# Obtaining the position and activiy modifiers from raw data
#-----------------------------------------------

# Getting the dataset with modifiers
datasetWM=dataset
datasetWM[,"Behavior"]=as.character(datasetWM[,"Behavior"])
datasetWM[,"Modifier_Exploration"]=as.character(datasetWM[,"Modifier_1"])
datasetWM[,"Modifier_Activity"]=as.character(datasetWM[,"Modifier_2"])
datasetWM[!is.na(datasetWM[,"Modifier_Activity"]),"Behavior"]=datasetWM[!is.na(datasetWM[,"Modifier_Activity"]),"Modifier_Activity"]
datasetWM[!is.na(datasetWM[,"Modifier_Exploration"]),"Behavior"]=datasetWM[!is.na(datasetWM[,"Modifier_Exploration"]),"Modifier_Exploration"]

# Adding Playing games and Reading in the Activity behaviors
datasetWM2=dataset
datasetWM2[,"Behavior"]=as.character(datasetWM2[,"Behavior"])
datasetWM2[,"Modifier_Exploration"]=as.character(datasetWM2[,"Modifier_1"])
datasetWM2[,"Modifier_Activity"]=as.character(datasetWM2[,"Modifier_2"])
datasetWM2[!is.na(datasetWM[,"Modifier_Activity"]) & datasetWM2[,"Modifier_Activity"]=="Reading","Behavior"]="Reading"
datasetWM2[!is.na(datasetWM[,"Modifier_Activity"])&datasetWM2[,"Modifier_Activity"]=="Playing Games","Behavior"]="Playing Games"

datasetWM2=datasetWM2[!is.na(datasetWM2[,"Behavior"])&datasetWM2[,"Subject"]%in%subjects_all&datasetWM2[,"Duration_sf"]!=0,c("Behavior","Subject","Time_Relative_sf","Duration_sf")]
datasetWM2[,"Subject"]=as.factor(as.character(datasetWM2[,"Subject"]))

#-------------------------------------------
# renaming subjects for figures in paper
#-------------------------------------------
newSubjectID=rep(NA,length(datasetWM2[,"Subject"]))
datasetWM2[,"Subject"]=as.character(datasetWM2[,"Subject"])
newSubjectID[substr(datasetWM2[,"Subject"],1,3)=="HC "]=paste0("H",substr(datasetWM2[substr(datasetWM2[,"Subject"],1,3)=="HC ","Subject"],4,6))
newSubjectID[substr(datasetWM2[,"Subject"],1,3)=="FTD"]=paste0("F",substr(datasetWM2[substr(datasetWM2[,"Subject"],1,3)=="FTD","Subject"],5, 7))
summary(factor(newSubjectID))

datasetWM2[,"Subject"]=as.factor(newSubjectID)
summary(datasetWM2[,"Subject"])
summary(factor(datasetWM2[,"Behavior"]))

subjects_new=levels(datasetWM2[,"Subject"])
subjectsFTD=subjects_new[substr(subjects_new,1,1)=="F"]
subjectsHC=subjects_new[substr(subjects_new,1,1)=="H"]
subjects_to_study=c(subjectsHC,subjectsFTD)

# At this stage, the dataset is ready to be analyzed

#---------------------------------------------
# Initializing list of attributes and colors
#---------------------------------------------
graphics.off()
listAttPosition=c("Standing","Sitting","Walking","Lying down","NAP")
listColorsPosition=c("Standing"="chocolate1","Sitting"="lightgoldenrod1","Walking"="lightgreen","Lying down"="red","NAP"="lightpink")
names(listColorsPosition)=listAttPosition
listAttActivity=c("Activity","Exploration","Non-activity","NAA","Transition")
listColorsActivity=c("Activity"="dodgerblue","Exploration"="forestgreen","Non-activity"="black","NAA"="deeppink","Transition"="lightgrey"  )
listAttActivityWM2=c(listAttActivity,"Reading","Playing Games")
listColorsActivityWM2=c(listColorsActivity,"Reading"="darkturquoise","Playing Games"="magenta4")

# visualize the colors
listColors=c(listColorsPosition,listColorsActivityWM2)
listAtt=c(listAttPosition,listAttActivityWM2)
plot.new()
legend(x="bottomleft",fill=listColors,legend=listAtt,cex=0.8)

#------------------------
# First graphical outputs (bandplot)
#------------------------
phases_to_use=c("FP1","FP2","FP3")
listAtt_to_use=c(listAttPosition,listAttActivityWM2)
listColors_to_use=c(listColorsPosition,listColorsActivityWM2)

# Pretreatment: checking that FREE phase corresponds to FP1 + FP2 + FP3
dataset_to_use=datasetWM2[!is.na(datasetWM2[,"Time_Relative_sf"]),c("Behavior","Subject","Time_Relative_sf","Duration_sf")]
dataset_to_use[,"Time_Relative_sf"]=as.numeric(dataset_to_use[,"Time_Relative_sf"])
dataset_to_use[,"Duration_sf"]=as.numeric(dataset_to_use[,"Duration_sf"])
bandplot(dataset_to_use,listAtt=c("FP1","FP2","FP3"),tlim=c(0,700) )
bandplot(dataset_to_use,listAtt=c("FREE"),tlim=c(0,700) )

listAttPositionWithoutNAP=listAttPosition[!listAttPosition=="NAP"]
listColorsPositionWithoutNAP=listColorsPosition[listAttPositionWithoutNAP]
datPosition=getMatrixForBigBandplot(dataset_to_use,listSubjects=c(subjectsFTD,subjectsHC),phases=c("FP1","FP2","FP3"),listAtt=listAttPositionWithoutNAP,listColors=listColorsPosition,behaviorColumn="Behavior",timeColumn="Time_Relative_sf",subjectColumn="Subject",durationColumn="Duration_sf")
graphics.off()
bandplot(datPosition,listAtt=listAttPositionWithoutNAP,listColors=listColorsPositionWithoutNAP, main="Free phase")

listAttActivityWM2WithoutNAA=listAttActivityWM2[!listAttActivityWM2=="NAA"]
listColorsActivityWithoutNAA=listColorsActivityWM2[listAttActivityWM2WithoutNAA]
datActivity=getMatrixForBigBandplot(dataset_to_use,listSubjects=c(subjectsFTD,subjectsHC),phases=c("FP1","FP2","FP3"),listAtt=listAttActivityWM2,listColors=listColorsActivityWM2,behaviorColumn="Behavior",timeColumn="Time_Relative_sf",subjectColumn="Subject",durationColumn="Duration_sf")
graphics.off()
bandplot(datActivity,listAtt=listAttActivityWM2WithoutNAA,listColors=listColorsActivityWithoutNAA,main="Free phase",rightmargin=7)

listAtt_to_use_wo=c(listAttPositionWithoutNAP,listAttActivityWM2WithoutNAA)
listColors_to_use_wo=c(listColorsPositionWithoutNAP,listColorsActivityWithoutNAA)

# with several phases
tiff(filename = "positionBandplot.tiff", units="in", width=6, height=5, res=300)
bandplot(datPosition,listAtt=listAttPositionWithoutNAP,listColors=listColorsPositionWithoutNAP, main="Free phase",rightmargin=8)
dev.off()
tiff(filename = "activityBandplot.tiff", units="in", width=6, height=5, res=300)
bandplot(datActivity,listAtt=listAttActivityWM2WithoutNAA,listColors=listColorsActivityWithoutNAA,main="Free phase",rightmargin=8)
dev.off()
graphics.off()

#----------------------------------------------------
# Clustering based on several distances
#------------------------------------------------------
listAtt_to_use=listAttActivityWM2
listAtt_to_use=c(listAttActivityWM2,listAttPosition)
method_agregation="ward.D2"
plot.new()

# #---- Option 0: durations
# res_0=distanceBetweenSubjects(dataset=dataset_to_use,subjects = subjectsFTD,phases=phases_to_use,listAtt=listAtt_to_use,conv1=NULL,method="duration")
# resmds0=plotMds(res_0,subjects=subjectsFTD,n_group=3,method=method_agregation)
# mds_gp0=resmds0$gp


#---- Option 1: the nearest neighbors distance
# opt_conv=1
# res_1=distanceBetweenSubjects(dataset=dataset_to_use,subjects = subjectsFTD,phases=phases_to_use,listAtt=listAtt_to_use,conv1=opt_conv,method="euclidean")
# resmds1=plotMds(res_1,subjects=subjectsFTD,n_group=3,method=method_agregation)
# mds_gp1=resmds1$gp
# plot(rev(1-(resmds1$reshclust$height/sum(resmds1$reshclust$height))),pch=16,xlab="Number of groups",ylab="Proportion of explained inertia",main="Explained inertia according to the number of groups")
# table(mds_gp0,mds_gp1)


#---- Option 2: DTW distance
# res_2=distanceBetweenSubjects(dataset=dataset_to_use,phases=phases_to_use,listAtt=listAtt_to_use,subjects=NULL,method="dtw")
# resmds2=plotMds(res_2,subjects=subjectsFTD,n_group=3,method=method_agregation)
# mds_gp2=resmds2$gp
# plot(rev(1-(resmds2$reshclust$height/sum(resmds2$reshclust$height))),pch=16,xlab="Number of groups",ylab="Proportion of explained inertia",main="Explained inertia according to the number of groups")

# #---- Option 3: levenshtein + ward
# res_3=distanceBetweenSubjects(dataset=dataset_to_use,phases=phases_to_use,listAtt=listAtt_to_use,subjects=NULL,method="lv")
# resmds3=plotMds(res_3,subjects=subjectsFTD,n_group=3,method=method_agregation)
# mds_gp3=resmds3$gp

#---- Option 4: Convolution windows size = time 
#---- Method implemented in the paper (Peltier et al., 2022)
#---- A temporal classification method based on behavior time series data
#---- in patients with behavioral variant of frontotemporal dementia and apathy.
opt_conv=400
res_4=distanceBetweenSubjects(dataset=dataset_to_use,subjects = subjectsFTD,phases=phases_to_use,listAtt=listAtt_to_use_wo,conv1=opt_conv,method="euclidean")
resmds4=plotMds(res_4,subjects=subjectsFTD,n_group=3,method=method_agregation)
mds_gp4=resmds4$gp
mds_gp4
tiff(filename = "MDS_results.tiff", units="in", width=6, height=5, res=300)
resmds4=plotMds(res_4,subjects=subjectsFTD,n_group=3,method=method_agregation)
dev.off()


# Classification 
res_dist=res_4
reshclust=hclust(as.dist(res_dist[c(subjectsFTD),c(subjectsFTD)]),method="ward.D2") # ward
reshclust_knn=hclust(as.dist(res_dist[c(subjectsFTD),c(subjectsFTD)]),method="single")# knn
plot(reshclust, cex.lab=0.75, cex.main=0.75)
tiff(filename = "classif.tiff", units="in", width=6, height=5, res=300)
plot(reshclust, cex.lab=0.75, cex.main=0.75)
dev.off()

plot(rev(1-(reshclust$height/sum(reshclust$height))),pch=16,xlab="Number of groups",ylab="Proportion of explained inertia",main="Explained inertia according to the number of groups",cex.main=0.75, cex.lab=0.75)
abline(h=0.9)
tiff(filename = "Explained_Inertia.tiff", units="in", width=6, height=5, res=300)
plot(rev(1-(reshclust$height/sum(reshclust$height))),pch=16,xlab="Number of groups",ylab="Proportion of explained inertia",main="Explained inertia according to the number of groups",cex.main=0.75, cex.lab=0.75)
abline(h=0.9)
dev.off()

# clustering and vizualizing group based on this distance
resmds=plotMds(res_dist,subjects=subjectsFTD,n_group=3)
resmds
tiff(filename = "mds.tiff", units="in", width=6, height=5, res=300)
resmds=plotMds(res_dist,subjects=subjectsFTD,n_group=3)
dev.off()

gp1_f=names(resmds$gp)[resmds$gp==1]
gp2_f=names(resmds$gp)[resmds$gp==2]
gp3_f=names(resmds$gp)[resmds$gp==3]
summary(factor(resmds$gp))
gp1_f
gp2_f
gp3_f

#-------------------------------------------------------
# Visualization of the behaviors in the obtained groups
#-------------------------------------------------------
graphics.off()
plot.new()
bandplots(dataset_to_use,listSubject=c(gp1_f,gp2_f,gp3_f,subjectsHC),phases=phases_to_use,standardization="left",listAtt=listAttActivityWM2,listColors = listColorsActivityWM2)
tiff(filename = "BandplotsActivityClassified.tiff", units="in", width=6, height=5, res=300)
bandplots(dataset_to_use,listSubject=c(gp1_f,gp2_f,gp3_f,subjectsHC),phases=phases_to_use,standardization="left",listAtt=listAttActivityWM2,listColors = listColorsActivityWM2)
dev.off()
bandplots(dataset_to_use,listSubject=c(gp1_f,gp2_f,gp3_f,subjectsHC),phases=c("FP1","FP2","FP3"),standardization="left",listAtt=listAttPosition,listColors = listColorsPosition)
tiff(filename = "BandplotsPositionsClassified.tiff", units="in", width=6, height=5, res=300)
bandplots(dataset_to_use,listSubject=c(gp1_f,gp2_f,gp3_f,subjectsHC),phases=c("FP1","FP2","FP3"),standardization="left",listAtt=listAttPosition,listColors = listColorsPosition)
dev.off()

graphics.off()
listAtt_to_use=listAtt
selectedSubjects=gp1_f
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 1")
tiff(filename = "Group1_Curves.tiff", units="in", width=8, height=3, res=300)
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 1")
dev.off()

selectedSubjects=gp2_f
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 2")
tiff(filename = "Group2_Curves.tiff", units="in", width=8, height=3, res=300)
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 2")
dev.off()


selectedSubjects=gp3_f
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 3")
tiff(filename = "Group3_Curves.tiff", units="in", width=8, height=3, res=300)
curves(dataset_to_use,selectedSubjects=selectedSubjects,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Group 3")
dev.off()

selectedSubjects=subjectsHC
curves(dataset_to_use,selectedSubjects=subjectsHC,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Healthy subjects")
tiff(filename = "HC_Curves.tiff", units="in", width=8, height=3, res=300)
curves(dataset_to_use,selectedSubjects=subjectsHC,phases_to_use,listColors_to_use_wo[names(listColors_to_use_wo)!="Transition"],listAtt_to_use_wo[listAtt_to_use_wo!="Transition"],offset=10,smoothing="medium",main="Healthy subjects")
dev.off()


#---------------------------
# Links with neuropsy data
#--------------------------

# Studying neuropsy data
# Comparing HC and FTD
datasetNPSI2[1,]
summary(factor(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD","SEX"]))
t1=table(datasetNPSI2[,"TYPE"],datasetNPSI2[,"SEX"])
chisq.test(t1)

var.quantis=c("AGE","YOE","MMSE","FAB","SAS","HAD_D","HAD_A","HAYL_ERR")
for(i in 1:length(var.quantis))
{
    var.quanti=var.quantis[i]
    datasetNPSI2[,var.quanti]=as.numeric(as.character(datasetNPSI2[,var.quanti]))
    print(var.quanti)
    wtest=wilcox.test(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD",var.quanti],datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC",var.quanti])
    print("FTD")
    print(summary(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD",var.quanti]),na.rm=T)
    print("HC")
    print(summary(datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC",var.quanti]),na.rm=T)
    print("pval")
    print(wtest$p.value)
}


# building the factor group
newSubjectIDnpsi=rep(NA,dim(datasetNPSI2)[1])
newSubjectIDnpsi[datasetNPSI2[,"TYPE"]=="HC"]=paste0("H",substr(datasetNPSI2[datasetNPSI2[,"TYPE"]=="HC","NAME"],4,6))
newSubjectIDnpsi[datasetNPSI2[,"TYPE"]=="FTD"]=paste0("F",substr(datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD","NAME"],5,7))
datasetNPSI2[,"Subject"]=newSubjectIDnpsi
datasetNPSI_FTD=datasetNPSI2[datasetNPSI2[,"TYPE"]=="FTD",]

group_ftd=rep(NA,length(datasetNPSI_FTD[,"NAME"]));
names(group_ftd)=datasetNPSI_FTD[,"Subject"]
group_ftd[names(group_ftd)%in%gp1_f]="gp1"
group_ftd[names(group_ftd)%in%gp2_f]="gp2"
group_ftd[names(group_ftd)%in%gp3_f]="gp3"

relevantVariables=c("AGE","YOE","MMSE","FAB","SAS","HAD_A","HAD_D","HAYL_ERR","Disinhibition")
for(i in 1:length(relevantVariables))
    {
    print(relevantVariables[i])
    varnpsi=as.numeric(datasetNPSI_FTD[,relevantVariables[i]])
    resanova=kruskal.test(varnpsi~as.factor(group_ftd))
    print("gp1")
    print(summary(varnpsi[group_ftd=="gp1"]))
    print("gp2")
    print(summary(varnpsi[group_ftd=="gp2"]))
    print("gp3")
    print(summary(varnpsi[group_ftd=="gp3"]))
    print( resanova$p.value)
    }
#  MMSE, HAD depression, and HAS anxiety are sig.

t=table(varnpsi=datasetNPSI_FTD[,"SEX"],group_ftd)
chisq.test(t)

# boxplots 
group_all=rep("HC",length(datasetNPSI2[,"NAME"]));
names(group_all)=datasetNPSI2[,"Subject"]
group_all[names(group_all)%in%gp1_f]="FTD1"
group_all[names(group_all)%in%gp2_f]="FTD2"
group_all[names(group_all)%in%gp3_f]="FTD3"

datasetNPSI2[,"MMSE"]=as.numeric(datasetNPSI2[,"MMSE"])
datasetNPSI2[,"HAD_A"]=as.numeric(datasetNPSI2[,"HAD_A"])
datasetNPSI2[,"HAD_D"]=as.numeric(datasetNPSI2[,"HAD_D"])

graphics.off()
df=data.frame(group=group_all,MMSE=datasetNPSI2[,"MMSE"])
p_mmse<-ggplot(data=df,aes(x=group,y=MMSE,color=group))+geom_boxplot()+geom_jitter()+ggtitle("MMSE")+theme_bw()
p_mmse
tiff(filename = "boxplot_mmse.tiff", units="in", width=4, height=4, res=300)
p_mmse
dev.off()

graphics.off()
df=data.frame(group=group_all,HAD_D=datasetNPSI2[,"HAD_D"])
p_had<- ggplot(data=df,aes(x=group,y=HAD_D,color=group))+geom_boxplot()+geom_jitter()+ggtitle("HAD_D")+theme_bw()
p_had
tiff(filename = "boxplot_HAD_D.tiff", units="in", width=4, height=4, res=300)
p_had
dev.off()

graphics.off()
df=data.frame(group=group_all,HAD_A=datasetNPSI2[,"HAD_A"])
p_haa<- ggplot(data=df,aes(x=group,y=HAD_A,color=group))+geom_boxplot()+geom_jitter()+ggtitle("HAD_A")+theme_bw()
p_haa
tiff(filename = "boxplot_HAD_A.tiff", units="in", width=4, height=4, res=300)
p_haa
dev.off()

graphics.off()
df=data.frame(group=group_all,Disinhibition=datasetNPSI2[,"Disinhibition"])
p_Disinhibition<- ggplot(data=df,aes(x=group,y=Disinhibition,color=group))+geom_boxplot()+geom_jitter()+ggtitle("Disinhibition")+theme_bw()
p_Disinhibition
tiff(filename = "boxplot_Disinhibition.tiff", units="in", width=4, height=4, res=300)
p_Disinhibition
dev.off()

#-------- End -------------------------------

