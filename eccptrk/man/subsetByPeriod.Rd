% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/subsetByPeriod.r
\name{subsetByPeriod}
\alias{subsetByPeriod}
\title{subsetByPeriod}
\usage{
subsetByPeriod(
  dataset,
  listSubject = NULL,
  phase = NULL,
  standardization = "left",
  binding = FALSE,
  phaseDuration = "no",
  timeColumn = "Time_Relative_sf",
  durationColumn = "Duration_sf",
  subjectColumn = "Subject",
  behaviorColumn = "Behavior"
)
}
\arguments{
\item{dataset}{Dataset with columns "Behavior","Subject","Time_Relative_sf","Duration_sf"}

\item{listSubject}{If NULL, all subjects are selected, if else, a vector containing a subset of subjects}

\item{phase}{name of a phase (detected in Behavior column) or NULL if no phase}

\item{standardization}{"no" or "left", or "leftright".}

\item{binding}{FALSE by default. If TRUE, similar phase in several parts are binded}

\item{phaseDuration}{"no" by default. If "minimal", the minimal value of duration is selected as the end of the phase}

\item{timeColumn}{Default to "Time_Relative_sf". Name of the column corresponding to the subject column}

\item{durationColumn}{Default to "Duration_sf". Name of the column corresponding to the subject column}

\item{subjectColumn}{Default to "Subject". Name of the column corresponding to the subject column}

\item{behaviorColumn}{Default to "Behavior". Name of the column corresponding to the subject column}
}
\value{
dataset
}
\description{
subsetByPeriod
}
\examples{
data_test=data.frame(Time_Relative_sf=rep(1:10,3),
Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
res=subsetByPeriod(dataset=data_test,listSubject=NULL,phase=NULL,
standardization="left", binding=FALSE,phaseDuration="no")
}
