#' returns a bandplot composed of several phases
#' @inheritParams bandplots
#' @param dataset_to_use a dataset containing behaviorColumn, timeColumn, subjectColumn and durationColumn
#' @param listSubjects a vector containing the names of the subjects to be displayed
#' @export
getMatrixForBigBandplot=function(dataset_to_use,listSubjects=NULL,phases=c("FP1","FP2","FP3"),listAtt=NULL,listColors=NULL,behaviorColumn="Behavior",timeColumn="Time_Relative_sf",subjectColumn="Subject",durationColumn="Duration_sf")
{
  if(is.null(listAtt)){listAtt=unique(dataset_to_use[,behaviorColumn])}
  if(is.null(listSubjects)){listSubjects=unique(dataset_to_use[,subjectColumn])}
  if(is.null(listColors)){listColors=rainbow(length(listAtt));names(listColors)=listAtt}
  res0=bandplots(dataset_to_use,listSubject=listSubjects,phases=phases,standardization="left",binding=TRUE,listAtt=listAtt,listColors=listColors,leftmargin=3)
  res=lapply(res0,function(x) return(x[x[,"Behavior"]%in%listAtt,]))
  resDataFP2=res[[2]]
  maxtime1=max(res[[1]][,"Time_Relative_sf"]+res[[1]][,"Duration_sf"])
  maxtime2=max(res[[2]][,"Time_Relative_sf"]+res[[2]][,"Duration_sf"])
  resDataFP1=res[[1]]
  resDataFP2[,"Time_Relative_sf"]=res[[2]][,"Time_Relative_sf"]+maxtime1
  resDataFP3=res[[3]]
  resDataFP3[,"Time_Relative_sf"]=res[[3]][,"Time_Relative_sf"]+maxtime1+maxtime2

  data_for_bandplot=rbind(resDataFP1,resDataFP2,resDataFP3)
  return(data_for_bandplot)
}