#' returns a matrice of distances from a list of matrices
#' @param listSubjects vector containing the names of the subjects
#' @param conv1 convolution window
#' @param method "euclidean" or levenshtein"
distanceFromList=function(listSubjects,conv1=c(1),method="euclidean")
{  if(is.null(dim(conv1))){convSize=1}
    else{convSize=apply(conv1,1,sum)}

    matDist=matrix(0,length(listSubjects),length(listSubjects));rownames(matDist)=names(listSubjects);colnames(matDist)=names(listSubjects)

    for(i in 1:length(listSubjects))
    {

        for(j in i:length(listSubjects))
        {
            if(j!=i)
            {

                matDist[i,j]=matricialDistance(listSubjects[[i]],listSubjects[[j]],method=method,convSize=convSize)
                matDist[j,i]=matDist[i,j]
            }

     }

    }
    return(matDist)
}
