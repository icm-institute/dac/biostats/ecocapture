#' summary.occurrenceTable
#'@param object result of occurrenceTable
#'@param resp response
#'@param ... Further parameters in print function
#'@examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' occtable=occurrenceTable(data_test)
#'@importFrom stats anova lm bartlett.test shapiro.test sd wilcox.test lm

summary.occurrenceTable=function(object,resp,...)
{
    pval=rep(NA,dim(object$dur)[2]);names(pval)=colnames(object$dur)
    fval=rep(NA,dim(object$dur)[2]);names(fval)=colnames(object$dur)
    mhc=rep(NA,dim(object$dur)[2]);names(mhc)=colnames(object$dur)
    mftd=rep(NA,dim(object$dur)[2]);names(mftd)=colnames(object$dur)
    sdhc=rep(NA,dim(object$dur)[2]);names(sdhc)=colnames(object$dur)
    sdftd=rep(NA,dim(object$dur)[2]);names(sdftd)=colnames(object$dur)
    
    for(i in 1:dim(object$dur)[2])
    {
      
        score=(object$dur)[,i]
        mhc[i]=mean(score[resp=="HC "]);sdhc[i]=sd(score[resp=="HC "])
        mftd[i]=mean(score[resp=="FTD"]);sdftd[i]=sd(score[resp=="FTD"])
        if(sd(score)!=0)
        {
            resLm=lm(  score~resp)
            resShapiro=shapiro.test(resLm$residuals)
            resBartlett=bartlett.test(score~resp)
            
            if( resShapiro$p.value>0.05 &&resBartlett$p.value>0.05)
            {
                resAnova=anova(resLm)   
                pval[i]=resAnova[1,5]
                print(paste(resAnova[1,5]),...)
            }
            else
            {
                wilcoxRes=wilcox.test(score[resp=="HC "],score[resp=="FTD"])
                pval[i]=wilcoxRes$p.value
                print(wilcoxRes$p.value)
            }
        }
        else
        {
            print("no variations")
        }
        
    }
    df=cbind(Mean_HC=mhc,Sd_HC=sdhc,Mean_FTD=mftd,Sd_FTD=sdftd,P_value=pval)
    return(df)  
}
