#' screening
#' Screening of the variables
#' @param gp1 first group to be tested
#' @param gp2 second group to be tested
#' @param relevantVariables vector of variable names to be tested (should be colnames of blocknpsi)
#' @param blocknpsi dataset whose colnames are neuropsy variables
#' @param threshold threshold of significance
#' @importFrom stats t.test
screening=function(gp1,gp2,relevantVariables,blocknpsi,threshold=0.1)
{
    for(vari in relevantVariables)
    {
        blocknpsi[,vari]=as.numeric(blocknpsi[,vari])
        #  print(vari)
        #  print(t.test(blocknpsi[gp1,vari],blocknpsi[gp2,vari])$p.value)
        
        if(sd(blocknpsi[gp1,vari],na.rm=T)==0||sd(blocknpsi[gp1,vari],na.rm=T)==0){print("no var in one group")}else{
            if(t.test(blocknpsi[gp1,vari],blocknpsi[gp2,vari])$p.value<threshold)
            {
                
                print(vari)
                print(t.test(blocknpsi[gp1,vari],blocknpsi[gp2,vari])$p.value)
            } 
            
        }
    } 
}