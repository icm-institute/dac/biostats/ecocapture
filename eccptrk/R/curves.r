#' plots the curves corresponding to
#' @param dataset_to_use dataset 
#' @param selectedSubjects vector containing the names of selected subjects
#' @param phases_to_use vector containing the names of the phases to use
#' @param listAtt_to_use vector containing the names of attributes to use
#' @param listColors_to_use vector containing the colors to use
#' @param main title of the plot
#' @param smoothing "none","low","medium" or "strong". Smoothing of the curves
#' @param subjectColumn Default to "Subject". Name of the column corresponding to the subject column
#' @param timeColumn  Default to "Time_Relative_sf". Name of the column corresponding to the subject column
#' @param durationColumn  Default to "Duration_sf". Name of the column corresponding to the subject column
#' @param behaviorColumn  Default to "Behavior". Name of the column corresponding to the subject column
#' @param offset offset for the legend
#' @examples 
#'  data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' curves(data_test)
#' @export
#' @importFrom graphics abline
curves=function(dataset_to_use,selectedSubjects=NULL,phases_to_use=NULL,listColors_to_use=NULL,listAtt_to_use=NULL,main="Curves",smoothing="strong",offset=0,subjectColumn="Subject",behaviorColumn="Behavior",timeColumn="Time_Relative_sf",durationColumn="Duration_sf")
{
    if(is.null(phases_to_use))
    {
        res_select=subsetByPeriod(dataset_to_use,listSubject = selectedSubjects, phase=NULL,standardization = "left",binding=TRUE,phaseDuration="minimal",subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)$dataset
        occur=occurrenceTable(res_select,listAtt=listAtt_to_use,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)
        plot(occur,smoothing=smoothing,listColors=listColors_to_use,main=main,offset=offset)
    }
    else
    {   
        occur=list()
        for(i in 1:length(phases_to_use))
        {
            res_select=subsetByPeriod(dataset_to_use,listSubject = selectedSubjects, phase=phases_to_use[i],standardization = "left",binding=TRUE,phaseDuration="minimal")$dataset
            occur[[i]]=occurrenceTable(res_select,listAtt=listAtt_to_use,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)[["occ"]]
        }
        occt=list()
        occt[["occ"]]=Reduce(cbind,occur)
        occt[["nsuj"]]=length(selectedSubjects)
        class(occt)="occurrenceTable"
        plot(occt,main=main,smoothing=smoothing,listColors=listColors_to_use,offset=offset)
        
    }
       abline(v=109,lty=2)
    abline(v=229,lty=2)
}