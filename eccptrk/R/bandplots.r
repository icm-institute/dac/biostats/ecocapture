#' Bandplots
#' @inheritParams bandplot 
#' @inheritParams subsetByPeriod
#' @param phases name of the phase to be plotted (or vector containing the names of the phases to be plotted). If NULL, no phase is considered
#' @export
#' @examples 
#'  data_test=data.frame(Time_Relative_sf=rep(1:10,3),Duration_sf=rep(1,30),
#' Subject=rep(c("a","b","c"),each=10),Behavior=sample(c("happy","sad","na"),30,
#' replace=TRUE))
#' bandplots(dataset=data_test)
#' @importFrom graphics plot.new
bandplots=function(dataset,phases=NULL,listAtt=NULL,standardization="left",binding=TRUE,listSubject=NULL,listColors=NULL,phaseDuration="minimal", main="Bandplot",onlyAttPresentInLegend=FALSE,offset=10,leftmargin=5,rightmargin=5,subjectColumn="Subject",behaviorColumn="Behavior",timeColumn="Time_Relative_sf",durationColumn="Duration_sf")
{
    datasetPhase=NULL
    if(length(phases)>1)
    {
        #nf <- layout(matrix(rep(c(1,1,2,2,3,3,3),7), 7, 7, byrow = TRUE), respect = TRUE)
        par(mfcol=c(1,length(phases)+1))
        for(i in 1:length(phases))
        {
            res=subsetByPeriod(dataset,listSubject=listSubject,phase=phases[i],standardization=standardization,binding=binding,phaseDuration=phaseDuration,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)
            datasetPhase[[phases[i]]]=res$dataset  
            bandplot(datasetPhase[[phases[i]]],listAtt=listAtt,listSubject=listSubject,listColors=listColors,tlim=NULL,main=phases[i],displayLegend=F,displaySubject=ifelse(i==1,TRUE,FALSE),onlyAttPresentInLegend=onlyAttPresentInLegend,offset=offset,leftmargin=leftmargin,rightmargin=0,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)
        }
        plot.new()
        par(mar=c(0,0,0,0))
        legend("topleft",legend=listAtt,fill=listColors)
    }
    else
    {
        res=subsetByPeriod(dataset,listSubject=listSubject,phase=phases,standardization=standardization,binding=binding,phaseDuration=phaseDuration,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)
        datasetPhase=res$dataset  
        bandplot(datasetPhase,listAtt=listAtt,listSubject=listSubject,listColors=listColors,tlim=NULL,main=main,displayLegend=T,displaySubject=T,onlyAttPresentInLegend=onlyAttPresentInLegend,offset=offset,leftmargin=leftmargin,rightmargin=rightmargin,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)
    }
    return(datasetPhase)
    
}